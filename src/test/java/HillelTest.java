import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.currentFrameUrl;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class HillelTest {

    @Test
    public void shouldPresentInfoOnTvPage() {
        openTvsGrid();
        ElementsCollection tvs = $$("a[class=\"product-link-to-pdp product-url\"]");
        SelenideElement element = tvs.get(new Random().nextInt(tvs.size()));
        String expectedUrl = element.attr("href");
        String expectedTvIdentifier = element.find("img[class*=\"js-product-img-main\"]").attr("alt");
        element.parent().click();
        String selector = "[alt=\"" + expectedTvIdentifier + "\"]";
        $(selector).should(exist);
        assertEquals(expectedUrl, currentFrameUrl());
        $("[class=\"jump-links inline\"] [data-link-label*=\"Specifications\"]").should(exist);
        $("[class=\"jump-links inline\"] [data-link-label*=\"Specifications\"]").click();
    }

    @Test
    public void shouldFilteringTvsOnGrid() {
        openTvsGrid();
        int origSize = $$(".product-price strong").exclude(hidden).size();
        $("[aria-label=Filter]").click();
        $("[aria-label=\"$0-$999\"]").click();
        ElementsCollection tvs = $$(".product-price strong").exclude(hidden);
        assertTrue(tvs.size() < origSize);

        for (SelenideElement el:tvs) {
            double val = Double.parseDouble(el.text().substring(1));
            assertTrue(val > 0 && val <= 999);
        }
    }

    @Test
    public void shouldHaveOnlineSellersPlaystion4() throws InterruptedException {
        open("https://www.sony.com/");
        assertEquals(url(), "https://www.sony.com/");
        $("[href*=\"electronics/playstation\"]").click();
        Thread.sleep(1500);
        assertEquals(url(),"https://www.sony.com/electronics/playstation");
        $("nav [href*=\"explore/ps4\"]").click();
        switchTo().window(1);
        Thread.sleep(1500);
        assertEquals(url(),"https://www.playstation.com/en-us/explore/ps4/");
        $("[target-tag*=\"buy-now\"]").click();
        $("[aria-label*=\"711719510024\"]").click();
        $("[aria-label*=\"box/popup\"]").should(appear);
        assertTrue($$("button[class*=\"ps-online-seller-button\"]").size() > 0);
    }

    @Test
    public void shouldCompareTwoToFiveRandomSelectedTvs() throws InterruptedException {
        openTvsGrid();
        List<String> expectedTvIdentifiers = new ArrayList<>();
        ElementsCollection checkboxes = $$("input[id*=\"sony:product\"]");
        int quantity = new Random().nextInt(4) + 2;
        for (int i=0; i < quantity; i++) {
            SelenideElement element = checkboxes.get(new Random().nextInt(checkboxes.size()));
            expectedTvIdentifiers.add(i, element.attr("id"));
            element.click();
            checkboxes = checkboxes.excludeWith(selected);
        }
        $("span[class*=compare-browse-btn]").click();
        Thread.sleep(1000);
        checkboxes = $$("div.compare-item").exclude(hidden);
        assertEquals(quantity, checkboxes.size());
        List<String> actualTvIdentifiers = new ArrayList<>();
        checkboxes.forEach(x -> actualTvIdentifiers.add(x.attr("data-compare-id")));
        assertTrue(expectedTvIdentifiers.containsAll(actualTvIdentifiers));
    }

    @Test
    public void shouldSortPrice() {
        openTvsGrid();
        int origSize = $$(".product-price strong").exclude(hidden).size();
        $("[aria-owns=sortByMenuItems]").click();
        $("[aria-label=\"Price - low to high\"]").click();
        assertEquals($$(".product-price strong").exclude(hidden).size(), origSize);
        assertEquals(url(), "https://www.sony.com/electronics/tv/t/televisions?sort=price(lowtohigh)");
        $("[aria-owns=sortByMenuItems]").click();
        $("[aria-label=\"Price - high to low\"]").click();
        assertEquals($$(".product-price strong").exclude(hidden).size(), origSize);
        assertEquals(url(), "https://www.sony.com/electronics/tv/t/televisions?sort=price(hightolow)");
        $("[aria-owns=sortByMenuItems]").click();
        $("[aria-label=\"New\"]").click();
        assertEquals($$(".product-price strong").exclude(hidden).size(), origSize);
        assertEquals(url(), "https://www.sony.com/electronics/tv/t/televisions?sort=newproducts");
        $("[aria-owns=sortByMenuItems]").click();
        $("[aria-label=\"Ratings\"]").click();
        assertEquals($$(".product-price strong").exclude(hidden).size(), origSize);
        assertEquals(url(), "https://www.sony.com/electronics/tv/t/televisions?sort=product_ratings");
        $("[aria-owns=sortByMenuItems]").click();
        $("[aria-label=\"Featured\"]").click();
        assertEquals($$(".product-price strong").exclude(hidden).size(), origSize);
        assertEquals(url(), "https://www.sony.com/electronics/tv/t/televisions?sort=featured");
    }

    private void openTvsGrid() {
        open("https://www.sony.com/");
        assertEquals(url(), "https://www.sony.com/");
        $("[data-target*=electronics]").click();
        $("[data-category-name=tvs]").shouldBe(visible);
        $("[data-category-name=tvs]").click();
        assertEquals(url(), "https://www.sony.com/electronics/tv/t/televisions");
    }
}
